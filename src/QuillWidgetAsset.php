<?php

namespace yourock\quill;

use yii\web\AssetBundle;

class QuillWidgetAsset extends AssetBundle
{
	public $sourcePath = __DIR__ . '/Assets';

	public $css = [
		"quill/quill.snow.css",
	];

	public $js = [
		"quill/quill.min.js"
	];

	public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

	public $depends = [];
}