<?php

namespace yourock\quill;

use yii\widgets\InputWidget;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Виджет для редактора Quill
 *
 * Class QuillWidget
 * @author yourock
 * @package yourock\quill
 */
class QuillWidget extends InputWidget
{
	/**
	 * Начальные данные
	 * @var string
	 */
	public $content;

	/**
	 * Генерация ID для input field
	 * @var string
	 */

	private $inputId;

	/**
	 * Использовать локальные ресурсы? если нет,то CDN
	 * @var boolean
	 */
	public $local = false;

	/**
	 * {@inheritDoc}
	 * @see \yii\widgets\InputWidget::init()
	 */
	public function init()
	{
		if ($this->model === null) {
			$this->initWidget();
		} else {
			$this->initInputWidget();
		}
	}

	private function initWidget()
	{
		Widget::init();
		if ($this->content === null) {
			$this->content = '';
		}
	}

	private function initInputWidget()
	{
		parent::init();
	}

	private function getInputId()
	{
		!$this->inputId ? $this->inputId = uniqid("editor") : '';

		return $this->inputId;
	}

	private function registerAssets()
	{
		$view = $this->getView();
		QuillWidgetAsset::register($view);
	}

	public function run()
	{
		$this->view->registerJs($this->buildScript(), $this->view::POS_READY);
		$this->registerAssets();

		if (!$this->model) {
			return Html::tag('div', $this->content, ['id' => $this->getInputId()]);
			Html::script($this->buildScript());
		}
	}

	public function buildScript()
	{
		return <<<marker
    var quill = new Quill('#{$this->getInputId()}', {
		theme: 'snow'
	});
marker;
	}
}