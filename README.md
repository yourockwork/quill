Quil editor widget для Yii2
========================
Тестовый виджет обертка редактора Quill https://quilljs.com
Выполнен в обучающих целях.
#### Данный редактор не поддерживается. 

Установка
------------
php composer.phar require "yourock/quill" "*"

Использование
-----

```
use yourock\quill\QuillWidget;
<?=  QuillWidget::widget(['content' => "some content"]); ?>
```

Опции
-----
* Поддерживаются следующие опции:   
	* content, string инициализация с заданным контентом, по умолчанию будет пусто;</li>